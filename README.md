#Ejercicio 1
def cantAparicionesSub(a,b):
    # inicializo contadores de componentes de a (i) y de b (j) 
    i=0
    j=0
    # inicializo contador de k
    k=0
    while i<len(a):
    # este ciclo termina cuando analizo todos los componentes de a
        while j<len(b):
        # este ciclo termina cuando analizo todos los componentes de b. comparo un componente de a con cada uno de los componentes de b 
        # si son iguales k aumenta en uno   
           if a[i]==b[j]:
	            k=k+1
                j=j+1
        # si no son iguales comparo con el componente de b siguiente   
           else:
	            j=j+1
        # una vez comparado con todos los componentes de b, paso al siguiente componente de a  y repito el ciclo
        i=i+1
		j=0
    # la funci�n cantAparicionesSub devuelve el valor final de k
    return k
# defino las dos listas de n�meros enteros
a=()
b=()
res=cantAparicionesSub(a,b)
print(res)